# Mattermost Chatbots

These Chatbots can be hosted on a server with PHP 7+.
The Application is written with the Slim Framework 3.

Following Chatbots are available:
- GiphyBot

## Settings

After you uploaded the files to your server, you have to modify `src/settings.php`

Replace the placeholder `PLACE_TOKEN_HERE` with your integration token provided by mattermost.
